#!/bin/bash

# Note: Running from "src" path / Application path
APP_PATH=$(pwd)

PATHS=( "resources/migrations/albums" "resources/migrations/sql" )

# --------------------------------------------------
# Settings variables for migrations (For production)
# --------------------------------------------------

for path in ${PATHS[*]}
do
  cd "$APP_PATH/$path"

  knex migrate:latest
done
