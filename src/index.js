const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./resources/events.json"))
    .addEventHandlers(require("./app/events-handlers"))
    .addCommands(require("./app/commands"))
    .addQueries(require("./app/queries"))
    .addServices(require("./app/services"))
    .addRepositories(require("./app/repo"))
    .withApi(require("./app/api"))
    .withTasks(require("./app/tasks"))
    .withUsers(require("./app/auth/users"))
    .withPermissions(require("./app/auth/permissions"))
    .withMessaging(require("./app/messaging"))
    .withSchemas(require("./app/schemas"))
    .withServer(require("./app/server"))
    .useFiles()
    .useInfo()
    .build();

  microservice.start();
}());
