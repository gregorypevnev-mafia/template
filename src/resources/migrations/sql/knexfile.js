const config = {
  // Connection-Data from Environment
};

module.exports = {
  client: "pg",
  connection: config,
  pool: {
    min: 2,
    max: 10
  }
};