exports.up = function (knex) {
  return knex.schema.createTable("users", builder => {
    builder.string("id").primary("USERS_PK");
    builder.string("email", 50).notNullable();
    builder.string("username", 50).notNullable();
    builder.string("password").notNullable();
    builder.timestamps(true, true);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("users");
};
