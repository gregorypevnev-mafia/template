exports.up = function (knex) {
  return knex.schema.createTable("albums", builder => {
    builder.increments("id").primary("ALBUMS_PK");
    builder.string("name").notNullable();
    builder.decimal("rating", 3, 1);
    builder.timestamps(true, true);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("albums");
};

// Important: Return - Promises / Async
