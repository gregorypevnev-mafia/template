module.exports = {
  ...require("./empty"),
  ...require("./data"),
  ...require("./dependencies"),
};