const datasourceProviders = require("../libs/datasources");

const loadProviders = () => datasourceProviders.reduce((providers, { name, creator }) => ({
  ...providers,
  [name]: creator,
}), {});

const prepareQueries = datasources => queries =>
  queries.map(({ type, datasource, handler }) => {
    const ds = datasources[datasource];

    if (!ds) {
      console.log(datasources, datasource);
      throw new Error("Datasource not found");
    }

    return {
      type,
      handler: ds.query(handler),
    }
  });

const prepareRepositories = datasources => repos =>
  repos.map(({ name, datasource, operations, transactional }) => {
    const ds = datasources[datasource];

    if (!ds) throw new Error("Datasource not found");

    return {
      name,
      operations: ds.repository({ operations, transactional }),
    }
  });

const loadDatasources = async datasourcesConfig => {
  const providers = loadProviders();

  const datasources = await Promise.all(
    Object.keys(datasourcesConfig)
      .map(name => {
        const { type, config } = datasourcesConfig[name];

        return { name, type, config };
      })
      .filter(({ type }) => !!providers[type])
      .map(({ name, type, config, }) => Promise.resolve(providers[type](name, config)))
  );

  const datasourceTable = datasources.reduce((table, datasource) => ({
    ...table,
    [datasource.name]: datasource,
  }), {});

  return {
    prepareQueries: prepareQueries(datasourceTable),
    prepareRepositories: prepareRepositories(datasourceTable),
  };
}

module.exports = { loadDatasources };