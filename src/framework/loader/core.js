const { createDomainEvents } = require("../libs/core/events");
const { createCommandBus } = require("../libs/core/commands");
const { createQueryBus } = require("../libs/core/queries");
const { createDomainTasks } = require("../libs/core/tasks");
const { emptyObject, isEmpty } = require("./helpers");

const emptyEvents = () => emptyObject(["on", "emit"]);
const emptyTasks = () => emptyObject(["register", "schedule"]);

const configureService = (functions, config) =>
  Object.keys(functions).reduce((configuredFunctions, functionName) => ({
    ...configuredFunctions,
    [functionName]: functions[functionName](config)
  }), {});

const createDomainServices = (services, config) =>
  services.reduce((serviceMap, { name, functions }) => {
    return {
      ...serviceMap,
      [name]: configureService(functions, config),
    }
  }, {});

const createRepositories = repositories => {
  return repositories.reduce((repos, { name, operations }) => ({
    ...repos,
    [name]: operations,
  }), {});
};

const core = ({
  config,
  services,
  repositories,
  events,
  commands,
  queries,
  logger,
  tasks,
}) => {
  return {
    dependencies() {
      return {
        config,
        services,
        repositories,
        events,
        commands,
        queries,
        logger,
        tasks,
      };
    },
    listenToEvents(listener) {
      events.register(listener);
    },
    emit({ type, payload }) {
      events.emit(type, payload);
    }
  }
};

const builder = (messageQueue, taskQueue) => {
  let domainEvents = emptyEvents();

  let configuration = {};
  let serviceLogger = console.log.bind(console);

  let domainServices = {};
  let dataRepositories = {};
  let domainTasks = emptyTasks();

  let tasks = [];
  let queries = [];
  let commands = [];
  let eventHandlers = [];

  const readDependencies = () => ({
    config: configuration,
    services: domainServices,
    logger: serviceLogger,
  });

  const writeDependencies = () => ({
    config: configuration,
    services: domainServices,
    repositories: dataRepositories,
    events: domainEvents,
    logger: serviceLogger,
  });

  return {
    setConfig(newConfiguration) {
      configuration = newConfiguration;
    },
    setLogger(newLogger) {
      serviceLogger = newLogger;
    },
    setEvents(events) {
      if (!messageQueue) throw new Error("Message Queue not provided");

      if (!isEmpty(events))
        domainEvents = createDomainEvents(events, messageQueue);
    },
    registerQueries(newQueries) {
      queries = newQueries;
    },
    registerCommands(newCommands) {
      commands = newCommands;
    },
    registerHandlers(newHandlers) {
      eventHandlers = newHandlers;
    },
    registerRepositories(repositories) {
      dataRepositories = createRepositories(repositories);
    },
    registerServices(services) {
      domainServices = createDomainServices(services, configuration);
    },
    registerTasks(newTasks) {
      if (!taskQueue) throw new Error("Tasks Queue not provided");

      domainTasks = createDomainTasks(taskQueue);

      tasks = newTasks;

      return () => domainTasks.initialize();
    },
    build() {
      const commandBus = createCommandBus();
      const queryBus = createQueryBus();

      commands.forEach(
        ({ type, handler }) =>
          commandBus.register(type, handler(writeDependencies()))
      );

      eventHandlers.forEach(
        ({ type, handler }) =>
          domainEvents.on(type, handler(writeDependencies()))
      );

      queries.forEach(
        ({ type, handler }) => {
          // Note: Query is a Domain-Object -> Should have access to services and logger
          return queryBus.register(type, handler(readDependencies()));
        }
      );

      tasks.forEach(({ processor, ...config }) => domainTasks.register({
        processor: processor(writeDependencies()),
        ...config,
      }));

      return core({
        config: configuration,
        services: domainServices,
        repos: dataRepositories,
        events: domainEvents,
        commands: commandBus,
        queries: queryBus,
        logger: serviceLogger,
        tasks: domainTasks,
      });
    },
  };
}

const loadCore = ({
  events: {
    mq,
  },
  tasks: {
    queue,
  }
}) => builder(mq, queue);

module.exports = { loadCore };
