const createDomainTasks = queue => {
  const tasks = {};
  const initialTasks = [];
  let initialized = false;

  return {
    register({ name, params, processor, auto }) {
      const task = queue.createTask(name, params, processor);

      if (auto) initialTasks.push(task);
      else tasks[name] = task;
    },
    schedule(name, data) {
      const task = tasks[name];

      if (!task) throw { message: "Task is not registered" };

      task.run(data);
    },
    initialize() {
      if (initialized) return;

      initialized = true;
      initialTasks.forEach(task => task.run());
    }
  }
}

module.exports = { createDomainTasks }; 