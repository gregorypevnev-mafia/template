const AVAILABLE_METHODS = ["get", "post", "delete", "put"];
const DEFAULT_METHOD = AVAILABLE_METHODS[0];
const UNIVERSAL_METHOD = "all";

const getMethod = rawMethod => {
  const method = rawMethod ? String(rawMethod).toLowerCase() : DEFAULT_METHOD;

  if (method === UNIVERSAL_METHOD) return "use"; // Use controller for all routes

  return AVAILABLE_METHODS.includes(method) ? method : DEFAULT_METHOD;
}

module.exports = { getMethod };