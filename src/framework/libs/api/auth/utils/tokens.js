const jwt = require("jsonwebtoken");

// Additional: COuld extend with Redis-Invalidation

exports.encodeToken = ({
  secret,
  session
}) => user => {
  try {
    return jwt.sign({ user }, secret, { expiresIn: session }).toString();
  } catch (e) {
    return null;
  }
};

exports.decodeToken = ({
  secret,
}) => token => {
  try {
    const data = jwt.verify(token, secret);

    return data ? data.user : null;
  } catch (e) {
    if (e.name === "TokenExpiredError") throw { message: "Token expired" };

    return null;
  }
};

exports.invalidateToken = ({ }) => token => { };