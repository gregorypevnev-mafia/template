const { extractToken } = require("./http");
const { encodeToken, decodeToken, invalidateToken } = require("./tokens");
const { defaultVerification } = require("./permissions");

const createAuthUtils = ({
  tokens: {
    secret,
    session,
  },
  http: {
    header,
    token,
  }
}) => ({
  extractToken: extractToken({ header, token }),
  encodeToken: encodeToken({ secret, session }),
  decodeToken: decodeToken({ secret }),
  invalidateToken: invalidateToken({}),
  defaultVerifyPermissions: defaultVerification,
});

module.exports = { createAuthUtils };