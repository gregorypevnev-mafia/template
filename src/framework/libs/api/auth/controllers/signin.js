const createSignInController = ({
  findUser,
  encodeToken,
}) => {
  const signin = ({ }) => async (req, res) => {
    try {
      const user = await Promise.resolve(findUser(req.body));

      const token = await Promise.resolve(encodeToken(user));

      return res.status(200).json({ user, token });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  };

  return {
    path: "/signin",
    schema: "signin",
    use: {
      method: "POST",
      controller: signin,
    }
  }
};

module.exports = { createSignInController };
