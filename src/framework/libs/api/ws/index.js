const { createController } = require("./controller");

// Not providing Tickets-API if tickets are NOT used
const createWS = (tickets, mode) => {
  return {
    controller: mode === "ticket" ? createController(tickets) : null,
    middleware: [],
    ws: true
  }
};

module.exports = { createWS };
