const name = "validation";

const createValidationMiddleware = schemas => {
  const handler = ({ }) => ({ schema }) => async (req, res, next) => {
    const validationSchema = schemas[schema];

    if (!validationSchema) return next();

    try {
      const data = await validationSchema.validate(req.body, { abortEarly: false }); // ALL ERRORS

      req.body = data;

      return next();
    } catch (error) {
      return res.status(400).json({
        message: "Invalid input",
        errors: error.errors,
      })
    }
  };

  return {
    name,
    handler,
  };
};

module.exports = { createValidationMiddleware };
