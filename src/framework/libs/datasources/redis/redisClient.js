const redis = require("redis");
const asyncRedis = require("async-redis");

const createRedisClient = ({ host, port }) => {
  const client = redis.createClient({ host, port });

  const asyncClient = asyncRedis.decorate(client);

  return asyncClient;
};


module.exports = { createRedisClient };