const PORT = Number(process.env.PORT || 3000);
const HOST = String(process.env.HOST || "0.0.0.0");

// Do NOT split into components - NOT worth it
module.exports = {
  server: {
    port: PORT,
    host: HOST
  },
};
