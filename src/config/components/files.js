module.exports = {
  secret: "SECRET",
  path: "/Users/mac/Desktop/Microservices/event-driven/files",
  ttl: 60 * 1000,
  allowedExtensions: ["jpg", "jpeg", "png", "tiff"],
  fileField: "avatar",
}