module.exports = {
  events: {
    // Kafka
    mq: {
      id: "posts", // Group-ID and Source
      host: "localhost:9091",
    },
  },
  tasks: {
    queue: {
      name: "posts",
      config: {
        redis: {
          port: 7000,
          host: "localhost",
        }
      }
    }
  }
}