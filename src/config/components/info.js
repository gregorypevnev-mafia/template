const os = require("os");

const HOST = String(os.hostname());
const ID = Number(process.pid);

module.exports = {
  name: "posts",
  host: HOST,
  id: ID,
};