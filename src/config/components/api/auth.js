module.exports = {
  tokens: {
    secret: "SECRET",
    session: 24 * 60 * 60,
  },
  http: {
    header: "Authorization",
    token: "JWT",
  }
};