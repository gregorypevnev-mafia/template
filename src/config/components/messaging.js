const MESSAGE_CHANNEL = "messages";
const CONTROL_CHANNEL = "control";

const CLOSE_EVENT = "close";
const INFO_EVENT = "info";

const CONNECTED_EVENT = "open-connection";
const DISCONNECTED_EVENT = "close-connection";

module.exports = {
  channels: {
    messageChannel: MESSAGE_CHANNEL,
    controlChannel: CONTROL_CHANNEL,
  },

  messages: {
    closeMessage: CLOSE_EVENT,
    infoMessage: INFO_EVENT,
  },

  events: {
    connectedEvent: CONNECTED_EVENT,
    disconnectedEvent: DISCONNECTED_EVENT,
  },

  store: {
    port: 6000,
    host: "localhost",
  },

  tickets: {
    ttl: 10,  // In Seconds
  },

  ws: require("./common/ws"),
};
