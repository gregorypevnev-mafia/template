module.exports = {
  type: "mongo",
  config: {
    host: "localhost",
    port: 27017,
    user: "greg",
    password: "pass",
    database: "posts",
    replicaSet: "rs"
  }
};