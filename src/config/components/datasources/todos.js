module.exports = {
  type: "service",
  config: {
    url: "https://jsonplaceholder.typicode.com/todos",
    timeout: 1000,
    circuitBreaker: {
      failureCount: 3,
      failureTimeout: 5000,
      retriesCount: 2,
      retriesTimeout: 100,
    }
  }
};