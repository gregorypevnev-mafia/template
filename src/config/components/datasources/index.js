module.exports = {
  // Note: Could use either Technology or Domain specific terms
  mongo: require("./mongo"),
  cache: require("./cache"),
  todos: require("./todos"),
  albums: require("./albums"),
  sql: require("./sql"),
};