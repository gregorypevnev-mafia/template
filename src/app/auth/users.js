const formatUser = ({ id, password, ...details }) => ({ id, ...details });

exports.findUser = ({ commands }) => async ({ email, password }) => {
  const user = await commands.call("signin", { email, password });

  if (user === null) throw { message: "User not found" };

  return formatUser(user);
};

exports.createUser = ({ commands }) => async data => {
  const user = await commands.call("signup", { data });

  if (user === null) throw { message: "User could not be created" };

  return formatUser(user);
};

exports.detailsForUser = ({ }) => user => formatUser(user); // No additional info

