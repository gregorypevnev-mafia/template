const { mapId } = require("../utils/formatting");

const COLLECTION = "posts";

const find = mongo => async id => {
  const post = await mongo.collection(COLLECTION).findOne({ _id: id });

  if (!post) throw new Error(`Post with ID "${id}" not found`);
  return mapId(post);
};

// Providing options directly - NO other way around...

const create = mongo => async ({ id, ...data }, opts) => {
  const { result } = await mongo.collection(COLLECTION).insertOne({ _id: id, ...data }, opts);

  if (result.n === 0) throw new Error("Post NOT inserted");
  if (result.ok !== 1) throw new Error("Insert failed");
};

const remove = mongo => async (id, opts) => {
  const { result } = await mongo.collection(COLLECTION).deleteOne({ _id: id }, opts);

  // NOT checking "n" - Removal should be stateless
  if (result.ok !== 1) throw new Error("Remove failed");
};

const update = mongo => async ({ id, ...data }, opts) => {
  const { result } = await mongo.collection(COLLECTION).updateOne(
    { _id: id },
    { $set: { _id: id, ...data } },
    { upsert: true, ...opts }
  );

  // NOT using "nModified" - Update could be stateless
  if (result.n === 0) throw new Error("Post NOT updated");
  if (result.ok !== 1) throw new Error("Update failed");
};

module.exports = {
  name: "posts",
  datasource: "mongo",
  transactional: true,
  operations: {
    create,
    find,
    remove,
    update,
  },
};