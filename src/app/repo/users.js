const TABLE = "users";

const findById = knex => async id => {
  const user = await knex(TABLE).select("*")
    .where({ id })
    .limit(1)
    .then(users => users[0]);

  if (!user) throw new Error(`User with ID "${id}" not found`);

  return user;
};

const findByUsername = knex => async username => {
  const user = await knex(TABLE).select("*")
    .where({ username })
    .limit(1)
    .then(users => users[0]);

  if (!user) throw new Error(`User with username "${username}" not found`);

  return user;
}

const create = knex => async ({ id, email, password, username }) => {
  const result = await knex(TABLE).insert({ id, email, password, username }).returning("id");

  if (result.length === 0) throw new Error("User NOT inserted");
};

const remove = knex => async id => {
  const { result } = await knex(TABLE).delete({ id }).returning("id");

  if (result.length === 0) throw new Error("User NOT removed");
};

module.exports = {
  name: "users",
  transactional: true,
  datasource: "sql",  // By Name
  operations: {
    create,
    findById,
    findByUsername,
    remove,
  },
};