const type = "signin";

const handler = ({
  repositories: { users },
  services: { passwords }
}) => async ({ email, password }) => {
  try {
    const user = await users.findByUsername(email);

    if (!user) return null;

    const result = await passwords.check(user.password, password);

    return result ? user : null;
  } catch (error) {
    console.log(error);
    throw { message: "Could not sign in", error }
  }
};

module.exports = {
  type,
  handler
};