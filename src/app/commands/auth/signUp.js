const type = "signup";

const handler = ({
  repositories: { users },
  services: { passwords, ids }
}) => async ({ data: { email, password } }) => {
  const userCheck = await users.findByUsername(email);

  if (userCheck) throw { message: "User with such email / username already exists" };

  try {
    const hashedPassword = await passwords.hash(password);

    const user = {
      id: ids.generate(),
      username: email,
      email: email,
      password: hashedPassword,
      at: Date.now(),
    };

    await users.create(user);

    await users.commit();

    return user;
  } catch (error) {
    await users.rollback();
    console.log(error);
    throw { message: "Could not sign up", error }
  }
};

module.exports = {
  type,
  handler
};