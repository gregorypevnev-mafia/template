const type = "create-post";

// Command and Events are separate operations -> Differnete Transactions
// Flow: 
//  1) Common:
//      1. Preparation (Params / Config)
//      2. Operations
//  2) Success:
//      1. Commit
//      2. Success-Event
//      3. Return Result
//  3) Error:
//      1. Rollback
//      2. Error-Event
//      3. Throw Error

// Note: Could have been using additional Service for Data-Access if needed - LARGE APPLICATION
const handler = ({
  events,
  repositories: { posts },
  services: { ids }
}) => async ({ data: { title, content }, user }) => {
  // 1.1
  const post = {
    id: ids.generate(),
    title,
    content,
    user: user.id,
    at: Date.now(),
  };

  try {
    // 1.2
    await posts.create(post);

    // 2.1
    await posts.commit();

    // 2.2
    events.emit("post-created", { post });

    // 2.3
    return post;
  } catch (error) {
    // 3.1
    await posts.rollback();

    // 3.2
    // Additional: Error-Event

    // 3.3
    throw { message: "Could not create post", error }
  }
};

module.exports = {
  type,
  handler
};