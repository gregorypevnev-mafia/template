const express = require("express");
const parser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");

const createServer = () => {
  const server = express();

  server.use(morgan("dev"));
  server.use(cors());
  server.use(parser.json());

  return server;
};

module.exports = createServer;