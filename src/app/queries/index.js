module.exports = [
  ...require("./posts"),
  ...require("./todos"),
  ...require("./albums"),
];