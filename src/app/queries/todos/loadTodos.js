const type = "load-todos";
const datasource = "todos";

const handler = datasource => ({ }) => async () => {
  const todosResult = await datasource({
    url: "/",
    method: "GET"
  });

  return todosResult.data;
};

module.exports = {
  type,
  datasource,
  handler,
};