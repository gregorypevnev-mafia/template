const type = "load-albums";
const datasource = "albums";

const handler = datasource => ({ }) => async () => {
  const albumsResult = await datasource.select("*").from("albums");

  return albumsResult;
};

module.exports = {
  type,
  datasource,
  handler,
};