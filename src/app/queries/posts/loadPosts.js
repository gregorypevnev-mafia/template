const { mapId } = require("../../utils/formatting");

const type = "load-posts";
const datasource = "mongo";

const LIMIT = 10;

const handler = datasource => ({ logger }) => async ({ search }) => {
  const posts = await datasource.collection("posts").find({
    title: { $regex: new RegExp(`.*${search}.*`) }
  }).limit(LIMIT).toArray();

  logger(`Posts for search "${search}"`, posts);

  const mappedPosts = posts.map(mapId);

  return mappedPosts;
};

module.exports = {
  type,
  datasource,
  handler,
};