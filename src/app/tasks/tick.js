const name = "tick";

// FLUSHALL to delete tasks
const params = {
  repeat: {
    every: 20000
  }
};

// Automatic tasks CANNOT have any arguments - Just use configuration or something
const processor = ({ logger }) => ({ }) => {
  logger("Tick");
};

module.exports = {
  name,
  params,
  processor,
  auto: true,
}