const name = "ping";

const params = {
  delay: 1000,
};

// Automatic tasks CANNOT have any arguments - Just use configuration or something
const processor = ({ logger }) => ({ message }) => {
  logger("Ping", message);
};

module.exports = {
  name,
  params,
  processor,
  auto: false, // By default
}