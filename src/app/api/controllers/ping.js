const ping = ({ tasks }) => async (req, res) => {
  tasks.schedule("ping", { message: "Hello" });

  return res.status(200).send("Pinged");
}

module.exports = {
  path: "/ping",
  use: {
    method: "POST",
    controller: ping
  }
}