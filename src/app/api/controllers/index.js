module.exports = {
  path: "/",
  use: [
    require("./posts"),
    require("./comments"),
    require("./todos"),
    require("./albums"),
    require("./ping"),
  ],
};