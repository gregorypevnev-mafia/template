const createPost = ({ commands }) => async (req, res) => {
  try {
    const { title, content } = req.body;

    const post = await commands.call("create-post", {
      data: {
        title,
        content,
      },
      user: req.user,
    });

    return res.status(201).json({ post });
  } catch (error) {
    return res.status(400).json({ error });
  }
};

const listPosts = ({ queries }) => async (req, res) => {
  const search = String(req.query.search || "");

  const posts = await queries.query("load-posts", { search });

  return res.status(200).json({ posts });
};

module.exports = {
  path: "/posts",
  middleware: [],
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "GET",
        controller: listPosts,
      }
    },
    {
      // "/" by default
      // NO MIDDLEWARE by default
      auth: true,
      private: true,
      use: {
        method: "POST",
        controller: createPost,
      }
    }
  ]
};
