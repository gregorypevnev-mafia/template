const listAlbums = ({ queries }) => async (req, res) => {
  const albums = await queries.query("load-albums");

  return res.status(200).json({ albums });
}

module.exports = {
  path: "/albums",
  use: {
    method: "get",
    controller: listAlbums
  }
}