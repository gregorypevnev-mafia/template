const { createProxyMiddleware } = require("http-proxy-middleware");

const PROXY_URL = "https://jsonplaceholder.typicode.com/comments";

const proxyComments = ({ }) => createProxyMiddleware({
  target: PROXY_URL,
  changeOrigin: true, // Chaneg "Host" to target
});

module.exports = {
  path: "/comments",
  use: {
    method: "all",
    controller: proxyComments,
  }
}