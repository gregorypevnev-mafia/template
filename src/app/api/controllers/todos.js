const listTodos = ({ queries }) => async (req, res) => {
  const todos = await queries.query("load-todos");

  return res.status(200).json({ todos });
}

module.exports = {
  path: "/todos",
  use: {
    method: "GET",
    controller: listTodos
  }
};