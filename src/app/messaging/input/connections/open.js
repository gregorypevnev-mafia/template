const name = "open-connection";

const pipe = ({ }) => ({ payload, user, room }) => {
  return {
    type: "user-joined",
    payload: {
      user,
      room,
      at: payload.at
    }
  }
};

module.exports = {
  name,
  pipe,
};
