const name = "close-connection";

const pipe = ({ }) => ({ payload, user, room }) => {
  return {
    type: "user-left",
    payload: {
      user,
      room,
      at: payload.at
    }
  }
};

module.exports = {
  name,
  pipe,
}