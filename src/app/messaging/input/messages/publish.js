const name = "publish-message";

const pipe = ({ }) => ({ payload, user, room }) => {
  const { title, body } = payload || {};

  if (!title || !body) return null;

  return {
    type: "message-published",
    payload: {
      message: {
        content: `${title} - ${body}`,
        by: user,
        in: room,
        at: Date.now(),
      }
    }
  }
};

module.exports = {
  name,
  pipe,
};
