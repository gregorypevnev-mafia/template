const name = "user-left";

const pipe = ({ }) => ({ user, room, at }) => {
  return {
    type: "user-disconnected",
    payload: { user, room, at }
    // Anycast
  }
};

module.exports = {
  name,
  pipe,
}
