const name = "user-joined";

const pipe = ({ }) => ({ user, room, at }) => {
  return {
    type: "user-connected",
    payload: { user, room, at }
    // Anycast
  }
};

module.exports = {
  name,
  pipe,
}
