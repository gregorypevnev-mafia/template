const name = "post-processed";

const pipe = ({ }) => ({ post }) => {
  return {
    type: "post-processed",
    payload: { post },
    user: post.user, // Unicast
  }
};

module.exports = {
  name,
  pipe,
}