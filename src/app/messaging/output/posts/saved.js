const name = "post-saved";

const pipe = ({ }) => ({ post }) => {
  return {
    type: "post-saved",
    payload: { post },
    user: post.user, // Unicast
  }
};

module.exports = {
  name,
  pipe,
};
