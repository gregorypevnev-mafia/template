module.exports = [
  ...require("./messages"),
  ...require("./posts"),
  ...require("./users"),
];
