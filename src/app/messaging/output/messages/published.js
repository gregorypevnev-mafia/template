const name = "message-published";

const pipe = ({ }) => ({ message }) => {
  return {
    type: "message-published",
    payload: {
      message,
    },
    room: message.in, // Broadcast
  }
};

module.exports = {
  name,
  pipe,
}
