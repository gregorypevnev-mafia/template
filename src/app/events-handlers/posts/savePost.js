const type = "post-processed";

const handler = ({ events, repositories: { posts } }) => async ({ post }) => {
  try {
    await posts.update(post);

    await posts.commit();
  } catch (error) {
    await posts.rollback();

    throw { message: "Could not save post", error }
  }

  events.emit("post-saved", { post });
};

module.exports = {
  type,
  handler
}