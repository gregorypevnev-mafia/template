const type = "post-created";

const handler = ({
  events,
  services: {
    slug
  },
  logger
}) => async ({ post }) => {
  try {
    const postSlug = slug.fromTitle(post.title);

    logger(`Slug for ${post.title}`, postSlug);

    // Sync - Waiting for emission
    await events.emit("post-processed", {
      post: {
        ...post,
        slug: postSlug,
      }
    });
  } catch (error) {
    // Error-Event
  }
};

module.exports = {
  type,
  handler
};