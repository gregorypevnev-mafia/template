const fs = require("fs");
const pathUtil = require("path");

const name = "files";

const checkFile = filename => new Promise((res, rej) => fs.exists(filename, res));

const download = ({ path }) => async filename => {
  const filepath = pathUtil.join(path, filename);

  const exists = await checkFile(filepath);

  if (!exists) throw { message: "File does not exist" };

  console.log("PATH:", filepath);

  return fs.createReadStream(filepath);
};

module.exports = {
  name,
  functions: {
    download
  }
};
