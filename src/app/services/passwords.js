const bcrypt = require("bcrypt");

const ROUNDS = 2;

const hash = ({ }) => async password => {
  const salt = await bcrypt.genSalt(ROUNDS);
  const hashed = await bcrypt.hash(password, salt);

  return hashed;
};

const check = ({ }) => async (hashedPassword, checkPassword) => {
  try {
    const result = await bcrypt.compare(checkPassword, hashedPassword);

    return result;
  } catch (e) {
    return false;
  }
};

module.exports = {
  name: "passwords",
  functions: {
    hash,
    check,
  }
};