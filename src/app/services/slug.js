// IMPORTANT: "g" is NECESSARY - Replacing multiple occurencies
const whitespace = new RegExp(" ", "g");
const specialChars = new RegExp("[!@#_]", "g");

const fromTitle = ({ }) => title =>
  String(title)
    .toLowerCase()
    .replace(whitespace, "-")
    .replace(specialChars, "");

module.exports = {
  name: "slug",
  functions: {
    fromTitle
  }
};