const crypto = require("crypto");

const name = "signer";

const ALLOWED_EXTENSIONS = ["jpg", "jpeg", "png", "tiff"];

const hash = ({ filename, extension, user, secret, expires }) =>
  crypto.createHash("sha256")
    .update(filename)
    .update(extension)
    .update(user)
    .update(secret)
    .update(String(expires))
    .digest("hex")
    .toString();

const createSignature = ({ secret, ttl }) => (file, user) => {
  const [filename, extension] = file.split(".");
  const expires = Date.now() + ttl;

  if (ALLOWED_EXTENSIONS.indexOf(extension) === -1) throw { message: "Invalid type of a file" };

  const signature = hash({ filename, extension, user, secret, expires });

  return { signature, expires };
};

const verifySignature = ({ secret }) => (filename, extension, user, expires, signature) => {
  const verificationSignature = hash({ filename, extension, secret, user, expires });

  if (verificationSignature !== signature) throw { message: "Invalid signature" };

  const timestamp = Date.now();

  if (timestamp > expires) throw { message: "Singature has expired" };
};

module.exports = {
  name,
  functions: {
    createSignature,
    verifySignature,
  }
};
