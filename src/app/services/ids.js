const { v1: uuid } = require("uuid");

// Each service has access to App-Config

const generate = ({ }) => () => String(uuid());

module.exports = {
  name: "ids",
  functions: {
    generate,
  }
};