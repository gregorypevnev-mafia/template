module.exports = [
  require("./slug"),
  require("./passwords"),
  require("./ids"),
  require("./signer"),
  require("./files"),
];
