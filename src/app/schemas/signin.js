const yup = require("yup");

const signinSchema = yup.object({
  email: yup.string().email("Not an email").required("Email is required"),

  password: yup.string().required("Password is required"),
});

module.exports = {
  name: "signin",
  schema: signinSchema,
};